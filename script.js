const galeri = document.querySelector(".resim-galerisi");
const resimYazilari = ["An OMG Particle's size is 0.0047 yactometer", 
                        "Resim 2 Açıklaması", 
                        "A quark's size is 1 attometer", 
                        "Resim 4 Açıklaması", 
                        "Resim 5 Açıklaması",
                        "Resim 6 Açıklaması",
                        "Resim 7 Açıklaması",
                        "Resim 8 Açıklaması",
                        "Resim 9 Açıklaması",
                        "Resim 10 Açıklaması",
                        "Resim 11 Açıklaması",
                        "Resim 12 Açıklaması",
                        "Resim 13 Açıklaması",
                        "Resim 14 Açıklaması",
                        "Small Artery 275 micrometer",
                        "Gran of sand 3mm",
                        "Mosquito 0.7cm",
                        "Dinoponera 3cm.",
                        "Humming bird 7 cm.",
                        "A squirell's size is 27 centimeter. 6 times smaller than you.",
                        "Approximatelly a persons height is 1.70.",
                        "3 story buiding is about 10 meters tall. 6 times bigger than you.",
                        "A320 37 meters long.",
                        "Empire State Building 380m.",
                        "Burj Khalifa tallest skyscraper is 828m tall",
                        "Lisbon 10km",
                        "Resim 27 Açıklaması",
                        "Resim 28 Açıklaması",
                        "Resim 29 Açıklaması",
                        "Resim 30 Açıklaması",
                        "Resim 31 Açıklaması",
                        "Resim 32 Açıklaması",
                        "Resim 33 Açıklaması",
                        "Resim 34 Açıklaması",
                        "Resim 35 Açıklaması",
                        "Resim 36 Açıklaması",
                        "Resim 37 Açıklaması",
                      ];

function galeriKaydirildi() {
  const kaydirmaPozisyonu = galeri.scrollLeft;

  // Her 500px kaydırmada resim index'ini ve yazıyı güncelle
  const yeniResimIndex = Math.floor(kaydirmaPozisyonu / 830);
  resimYazisiGoster(yeniResimIndex);
}

function resimYazisiGoster(resimIndex) {
  const resimYaziAlani = document.querySelector(".resim-yazisi");
  resimYaziAlani.textContent = resimYazilari[resimIndex];
}

// Galeriyi 3. resme kaydır
galeri.scrollLeft = 20 * 855;

// Galeri kaydırıldığında olay dinleyicisini ekleyin
galeri.addEventListener("scroll", galeriKaydirildi);

// Sayfa yüklendiğinde 3. resmin altına yazı ekler
window.addEventListener("load", () => {
  resimYazisiGoster(19);
});
